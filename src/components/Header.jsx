import React from "react";

function Header() {
  return (
    <div className="max-width">
      <section>
        <header className="header">
          <div>
            <h1>Where in the world ?</h1>
          </div>
          <div>
            <i className="fs fa moon"></i>Dark mood
          </div>
        </header>
      </section>
    </div>
  );
}

export default Header;
