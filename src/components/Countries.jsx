import React, { useEffect, useState } from "react";
// import { Link } from "react-router-dom";

function Countries() {
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    fetchCountryData();
  }, []);
  const fetchCountryData = async () => {
    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      setCountries(data);
      console.log(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  return (
    <>
      <div className="max-width">
        <section className="allCountryDiv">
          {countries.map((country, index) => {
            const {
              name,
              flags: { png: flags },
              population,
              region,
              capital,
              ccn3,
            } = country;

            return (
              <article key={index}>
                <div className="countryCard">
                  <img className="image" src={flags} alt={name.common} />
                  <div className="details">
                    <h3>{name.common}</h3>
                    <h4>
                      Population: <span>{population}</span>
                    </h4>
                    <h4>
                      Region: <span>{region}</span>
                    </h4>
                    <h4>
                      Capital: <span>{capital}</span>
                    </h4>
                  </div>
                </div>
              </article>
            );
          })}
        </section>
      </div>
    </>
  );
}

export default Countries;
