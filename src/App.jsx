import "./App.css";
import Filter from "./Filter";
import Countries from "./components/Countries";
import Header from "./components/Header";
function App() {
  return (
    <>
      <Header />
      <Filter/>
      <Countries />
    </>
  );
}

export default App;
